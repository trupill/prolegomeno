{-# language GADTs #-}
{-# language PolyKinds #-}
{-# language KindSignatures #-}
{-# language RankNTypes #-}
{-# language TypeInType #-}
{-# language TypeOperators #-}
{-# language StandaloneDeriving #-}
{-# language QuantifiedConstraints #-}
{-# language MultiParamTypeClasses #-}
{-# language FlexibleInstances #-}
{-# language TypeFamilies #-}
{-# language TypeApplications #-}
module Data.LambdaTerm (
  (:->:)
, Term(..)
, Name
, Args(..)
, bind
) where

import Data.Kind
import Data.Typeable hiding ((:~:))
import Generics.Kind hiding (Var)
import Unbound.Generics.LocallyNameless
import Unbound.Generics.LocallyNameless.Kind.Derive

infixr 0 :->:
data (p :->: q)

-- the language

data Term (c :: * -> *) t  where
  Lam  :: Bind (Name (Term c a)) (Term c b) -> Term c (a :->: b)
  App  :: Term c t ->  Args c t s -> Term c s
  Var  :: Name (Term c t) -> Term c t
  Con  :: c t             -> Term c t

infixr 5 :.:
data Args (c :: * -> *) (h :: *) (t :: *)  where 
  Nil   :: Args c t t
  (:.:) :: Term c x -> Args c r t -> Args c (x :->: r) t

-- boring stuff

deriving instance (forall t. Show (c t)) => Show (Term c t)
instance Split (Term c t) Term (c :&&: t :&&: LoT0)
instance GenericK Term (c :&&: t :&&: LoT0) where
  type RepK Term =
    (E {- a -} (E {- b -} (
      (V3 :~: ((:->:) :$: V1 :@: V0))
      :=>:
      (F (Bind :$: (Name :$: (Term :$: V2 :@: V1)) :@: (Term :$: V2 :@: V0))) )))
    :+:
    (E {- t -} (
      (F (Term :$: V1 :@: V0) :*: F (Args :$: V1 :@: V0 :@: V2))))
    :+:
    (F (Name :$: (Term :$: V0 :@: V1)))
    :+:
    (F (V0 :@: V1))
  
  fromK (Lam  b)   = L1 (E (E (C (F b))))
  fromK (App  x y) = R1 (L1 (E (F x :*: F y)))
  fromK (Var  x)   = R1 (R1 (L1 (F x)))
  fromK (Con  x)   = R1 (R1 (R1 (F x)))

  toK (L1 (E (E (C (F b)))))      = Lam b
  toK (R1 (L1 (E (F x :*: F y)))) = App x y
  toK (R1 (R1 (L1 (F x))))        = Var x
  toK (R1 (R1 (R1 (F x))))        = Con x

deriving instance (forall t. Show (c t)) => Show (Args c h t)
instance Split (Args c h t) Args (c :&&: h :&&: t :&&: LoT0)
instance GenericK Args (c :&&: h :&&: t :&&: LoT0) where
  type RepK Args =
    ((V1 :~: V2) :=>: U1)
    :+:
    (E {- x -} (E {- r -} (
      (V3 :~: ((:->:) :$: V1 :@: V0))
      :=>:
      (F (Term :$: V2 :@: V1) :*: F (Args :$: V2 :@: V0 :@: V4)) )))
  
  fromK Nil        = L1 (C U1)
  fromK (x :.: xs) = R1 (E (E (C (F x :*: F xs))))

  toK (L1 (C U1))                     = Nil
  toK (R1 (E (E (C (F x :*: F xs))))) = x :.: xs

instance (Typeable c, Typeable t, forall s. (Alpha (c s), Typeable s))
         => Alpha (Term c t) where
  aeq'        = aeqDefK
  fvAny'      = fvAnyDefK
  close       = closeDefK
  open        = openDefK
  isPat       = isPatDefK
  isTerm      = isTermDefK
  isEmbed     = isEmbedDefK
  nthPatFind  = nthPatFindDefK
  namePatFind = namePatFindDefK
  swaps'      = swapsDefK
  lfreshen'   = lfreshenDefK
  freshen'    = freshenDefK
  acompare'   = acompareDefK

instance (Typeable c, Typeable h, Typeable t, forall s. (Alpha (c s), Typeable s))
         => Alpha (Args c h t) where
  aeq'        = aeqDefK
  fvAny'      = fvAnyDefK
  close       = closeDefK
  open        = openDefK
  isPat       = isPatDefK
  isTerm      = isTermDefK
  isEmbed     = isEmbedDefK
  nthPatFind  = nthPatFindDefK
  namePatFind = namePatFindDefK
  swaps'      = swapsDefK
  lfreshen'   = lfreshenDefK
  freshen'    = freshenDefK
  acompare'   = acompareDefK