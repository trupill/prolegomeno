{-# language GADTs #-}
{-# language StandaloneDeriving #-}
{-# language QuantifiedConstraints #-}
module Data.LambdaTerm.Unification where

import Data.Set (Set)
import qualified Data.Set as S
import Data.Typeable
import Unbound.Generics.LocallyNameless
import Unbound.Generics.LocallyNameless.Name

import Data.LambdaTerm

-- follows https://github.com/jozefg/higher-order-unification/blob/master/explanation.md

data SomeVar c where
  SomeVar :: Name (Term c s) -> SomeVar c

instance Eq (SomeVar c) where
  SomeVar (Fn x y) == SomeVar (Fn u v) = (x, y) == (u, v)
  SomeVar (Bn x y) == SomeVar (Bn u v) = (x, y) == (u, v)
  _                == _                 = False
instance Ord (SomeVar c) where
  SomeVar (Fn x y) <= SomeVar (Fn u v) = (x, y) <= (u, v)
  SomeVar (Bn x y) <= SomeVar (Bn u v) = (x, y) <= (u, v)
  SomeVar (Fn _ _) <= SomeVar (Bn _ _) = True
  SomeVar (Bn _ _) <= SomeVar (Fn _ _) = False

{-
reduce :: Term c t -> Term c t
reduce (Lam b) = runFreshM $ do (x, body) <- unbind
                                return (bind x (reduce body))
reduce v@(Var  _) = v
reduce m@(Meta _) = m
reduce k@(Con  _) = k
reduce (App h r) = case reduce h of
  App h' r' -> reduce (App h' (append r' r))
  Lam b     -> undefined -- difficult case
  h'        -> App h' (reduceAll r)
-}