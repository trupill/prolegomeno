{-# language GADTs #-}
{-# language PolyKinds #-}
{-# language KindSignatures #-}
{-# language RankNTypes #-}
{-# language TypeInType #-}
{-# language TypeOperators #-}
{-# language StandaloneDeriving #-}
{-# language QuantifiedConstraints #-}
{-# language MultiParamTypeClasses #-}
{-# language FlexibleInstances #-}
{-# language TypeFamilies #-}
{-# language TypeApplications #-}
module Data.LambdaTerm.Example where

import Data.Kind
import Data.Typeable hiding ((:~:))
import Generics.Kind hiding (Var)
import Unbound.Generics.LocallyNameless
import Unbound.Generics.LocallyNameless.Kind.Derive

import Data.LambdaTerm

data LAMBDA

data CON a where
  LAM :: CON ((LAMBDA :->: LAMBDA) :->: LAMBDA)
  APP :: CON (LAMBDA :->: LAMBDA :->: LAMBDA)

-- constructors

lam :: (forall s. (Alpha (CON s), Typeable s))
    => Name (Term CON LAMBDA) -> Term CON LAMBDA -> Term CON LAMBDA
lam x e = App (Con LAM) (Lam (bind @(Name (Term CON LAMBDA)) @(Term CON LAMBDA) x e) :.: Nil)
app :: Term CON LAMBDA -> Term CON LAMBDA -> Term CON LAMBDA
app x y = App (Con APP) (x :.: y :.: Nil)

-- Boring stuff

deriving instance Show (CON a)
instance Split (CON a) CON (a :&&: LoT0)
instance GenericK CON (a :&&: LoT0) where
  type RepK CON =
    ((V0 :~: Kon ((LAMBDA :->: LAMBDA) :->: LAMBDA)) :=>: U1)
    :+: ((V0 :~: Kon (LAMBDA :->: LAMBDA :->: LAMBDA)) :=>: U1)

  fromK LAM = L1 (C U1)
  fromK APP = R1 (C U1)

  toK (L1 (C U1)) = LAM
  toK (R1 (C U1)) = APP

instance Alpha (CON a) where
  aeq'        = aeqDefK
  fvAny'      = fvAnyDefK
  close       = closeDefK
  open        = openDefK
  isPat       = isPatDefK
  isTerm      = isTermDefK
  isEmbed     = isEmbedDefK
  nthPatFind  = nthPatFindDefK
  namePatFind = namePatFindDefK
  swaps'      = swapsDefK
  lfreshen'   = lfreshenDefK
  freshen'    = freshenDefK
  acompare'   = acompareDefK